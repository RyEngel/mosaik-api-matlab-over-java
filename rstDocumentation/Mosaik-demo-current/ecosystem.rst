====================
The mosaik ecosystem
====================

.. figure:: /_static/mosaik-ecosystem.*
   :width: 600
   :align: center
   :alt: mosaik is only a co-simulation library. The simulators and tools
         implementing the Sim API form the mosaik ecosystem.

   Mosaik is only a co-simulation library. The simulators and tools
   implementing the Sim API form the mosaik ecosystem. This ecosystem is,
   what makes mosaik actually useful.
