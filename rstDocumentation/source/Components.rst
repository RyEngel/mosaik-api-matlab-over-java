==========
Components
==========

In this section, we list the existing components and refer to their documentaries as necessary.
Java, Python and MATLAB are naturally a prerequisite and assumed to be known.

mosaik
======

Mosaik is a python-based simulation environment for energy networks used to coordinate simulators in a common context.
In this article I will assume that the reader has a basic familiarity with mosaik and its working principles.
A full documentation of mosaik can be found  `here <https://mosaik.readthedocs.org/en/latest/overview.html>`_.


Java API
========
Starting 2014, Stefan Scherfke (stefan.scherfke@offis.de) has written and published a java application interface for mosaik. The here presented matlab interface can be seen as an extension to that Java API.
The Java API provides an abstract base class Simulator. It can be used to create an inheriting simulator class that overwrites the methods in Java's Simulator-class.
In order to use the Java API to communicate with MATLAB, we have written the Java Class MatlabSimulator inheriting from Simulator. When called, this class creates a connection to MATLAB using matlabcontrol. It overwrites all the methods provided by Java's Simulator-class in such a way that all input parameters are passed to a corresponding abstract MATLAB-class, also called Simulator.
The abstract Simulator-class in MATLAB can be used in the same way as she abstract simulator-class in Java.
The Java API can be found online `here <https://bitbucket.org/mosaik/mosaik-api-java>`_.



matlabcontrol
=============

Matlabcontrol is a MATLAB/Java API that allows for calling MATLAB from Java.
It works by creating a proxy object in Java. This object has the methods eval and returningEval, which allow the calling of
non-returning or returning MATLAB functions respectively.
The Java-class MatlabSimulator creates such a proxy (proxy name is matlab) in its constructor.
It calls MATLAB-functions later in the manner of::

        MatlabSimulator.matlab.eval("my_matlab_function("+ <Argument_from_Java>
        +");")


Not all types of variables work for input and output arguments. For remarks on which variable types are valid, see the documentation of the 'MatlabSimulrtor'-class below.
A general documentation of matlabcontrol can be found `here <https://code.google.com/p/matlabcontrol>`_.




Jsonlab
=======

Jsonlab is a freely available MATLAB toolbox for the handling of data in the json format. The abstract Simulator class in MATLAB uses this toolbox to convert the json-inputs from Java into MATLABs struct-format and vice versa.

The base toolbox is available from the `mathworks file exchange <http://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-file>`_ . 
However, the toolbox-scripts loadjson.m and savejson.m have been modifed for the current version of the MATLAB-API to be compatible with MATLABs containers.map datatype.
