=================================
Step-by-Step Implementation Guide
=================================


In mosaik, every model is associated with a Simulator. To implement a MATLAB-based mo-
saik model, we will therefore need a respective MATLAB-based simulator. One simulator can
control an arbitrary amount of models, but every model is associated with only one simulator.
The MATLAB-API communicates via calls to the simulator class methods. The first step will
therefore be the creation of a suitable simulator class. For this purpose, the MATLAB-API
provides the abstract simulator class. Every MATLAB-simulator should be realized inheriting
from this.

How does the model fit into the available methods?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The first consideration when implementing a new simulator should be, how the simulators
functionality translates to the available API calls, i.e. the methods of the abstract simulator
class. These are the constructor, init, create, step, getData and delete. All these functions
are already programmed for a normal simulator scenario. See section 4.1.1 for details on these
methods. If the standard functionality does not fit to the particular model(s), any of these
methods can be overwritten or modified in the inheriting simulator class. Usually however, the
standart simulator-methods should need no further editing. Only the metastruct variable must
always be specified.

Which parameters will be visible to mosaik?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Mosaik uses the metastruct parameter, which holds the information, which attributes are
accessible by the getData function and which parameters it needs upon initialization. The
metastruct must be returned by the init method, but I usually already define it in the simu-
lator constructor. A correct initialization of a metastruct-variable can look like this ::

        1 <model> meta = struct(); %Define Metadata for -model
        2 <model> meta.public = true;
        3 <model> meta.params = {'init val'};
        4 <model> meta.attrs = {'started','conn entity','Power','type','Eid','Energy'};
        5 <model> meta.any inputs = true;
        6
        7 <simulator-object>.metastruct.models = struct('<simulator-name>',<model> _meta); %Writing the Metadata to the metastruct

With this metastruct, the simulator <simulator-name> will control the model <model-name>.
In the init-call, mosaik can pass the variable init val and the attributes 'started', 'conn entity',
'Power', 'type', 'Eid','Energy' will be available to the getData-method. If the simulator
provides more than one model, the metastruct.models struct should naturally contain a cor-
responding metastruct for each model.

How do I create a simulator?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Optimally, your simulator class can just inherit from the abstract simulator class and needs no
further changes except for the parameter-settings in the constructor. If any method needs to be
modified, it can simply be specified in the inheriting class, overwriting th^e parent method. If
the modification is additional to the standard method, consider invoking the parent method
(e.g. obj@Simulator(simName) ), or simply copying the code into the overwriting method and
modifying it there.

How do I create my model?
^^^^^^^^^^^^^^^^^^^^^^^^^

This class describes the behavior of the implemented model. I suggest inheriting from the
provided abstract class model. Use the properties of your model class to save parameters between
calls. Make sure that all properties that are to be accessed by mosaik have a public reading
permission. Further, the parameter type must be set to the same string that is used in the
simulators metastruct. I suggest using the models classname.
The step()-method of the model defines the runtime-behavior of the model. Its input-parameters
will come from its connections to other models (and as answers to previous asynchronous calls
from this model). I recommend using the unravel inputs method to extract the input values
from the container.map.
step() only returns its asynchronous requests. All other results should be simply written to the
models parameters.

How do I set sim config?
^^^^^^^^^^^^^^^^^^^^^^^^

Once the MATLAB-simulator is complete to the degree that it fits into the structure of the API-
calls, it can be used by mosaik just like a java-based simulator. Set the sim config parameter
of mosaik like so ::

        1 sim config = { 
        2    'MAT-FCSimulator': { 
        3       'cmd': 'java -jar MatSim.jar %(addr)s FCSimulator', 
        4       'cwd': 'H:\\mosaik-api-matlab-over-java\\MATLAB-API-DEMO\\matlab- elements' 
        5    } 
        6 }


How can I create asynchronous requests?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Because both the json-format used by mosaik and the containers.map-format used by the MATLAB-API are rather unwieldy for easy use, the abstract model-class provides the createAsyncRequest-method, which can create an asynchronous request in the correct format from simple parameters.
mosaik will process all the asynchronous requests of the simulator as soon as the simulator.step()
method returns.


Special case: How can I submit a getData request and get an immediate answer?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A scenario is conceivable in which a model needs an input from a difierent model, but this cannot
be managed as usual via the mosaik-connect procedure. A reason might be, that the model only
determines during its step()-call, which further parameter it needs from what model. This is
possible with the correct implementation of a getData request. In that case, the step()-methoud
should basically be looking like this: ::

        1 function asyncs = step(battery , inputs ,step size , iteration)
        2 asyncs = containers.Map();
        3 switch iteration
        4 case 1
        5       thisModel.call me again = true;
        6        % calculate stuff for the model
        7       asyncs = thisModel.createAsyncRequest(asyncs ,'get',...
        8             <Target Model>,<RequestedAttribute>);
        9 case 2
        10      [entityname absender , val in , ~] = ...
        11              unravel inputs(thisModel , inputs , 'val in');
        12      % calculate stuff for the model
        13      thisModel.call me again = false;
        14 otherwise
        15      warning('Invalid Iteration attempt. Is something wrong?')
        16      thisModel.call me again = false;
        17 end

As you can see in the above code and figure, the iteration-procedure allows you to break
up your models step-method in several parts and perform asynchronous requests in between.
Javas MatlabSimulator class will call your MATLAB-simulator as long as at least one model of
that simulator has set its call me again-parameter to true.

.. figure:: /MatAPIdocstatic/async_datenflussV2.jpg
    :width: 800
    :align: center
    :alt: Step call to the MATLAB API with in-step getDatarequest
    
    Dataflow of the MATLAB-API if a getData request has to be processed within the step call usint the iteration-procedure.

A Necessity: Pre-initiating the MATLAB-session(s)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Every MATLAB-simulator has to run in its own session of MATLAB. In theory, invoking a
MATLAB-simulator via MatSim.jar automatically starts a new MATLAB-session (or reconnects
to one that was already opened from Java).
However, the long starting-time of the MATLAB-environment is prone to cause errors if it is
handled like that, because mosaik proceeds to using the simulator before the MATLAB-session
is truly ready, resulting an error (see Appendix B.2.3).
I have developed a method of circumventing this problem by starting the necessary number
of matlab-sessions before the actual simulators are invoked. To use this method, include the
following lines to the ' main ' function in python: ::

        1 no mat sessions = 3 #number of MATLAB sessions needed (= number of matlab simulators)
        2 debug matlab = True #open matlab hidden (for quick running) or openly (for debugging) ...
               call me again is false by default, but every model is called if it has an input ...
               assigned to it and iteration==1. 
        3 cwd = sim config.get('MAT-FCSimulator').get('cwd') #matlab folder path
        4 if(debug matlab):
        5 debug param = "debug"
        6 else:
        7 debug param = "nodebug"
        8
        9 #calls the Script that calls the java class that preopens MATLAB , then
        waits for initialization to complete
        10 call(cwd +"\\StartMatlab" + " " + cwd + " " + str(no mat sessions) + "" + debug param , shell = True)
        11 print('Waiting for MATLAB to get ready')
        12 time.sleep(2)

This simply calls a cmd-script StartMatlab.cmd with some parameters (see below). That script
starts a Java-script StartMatlab.class which then starts the necessary Matlab sessions.

        * Set debug matlab to false to open the matlab-sessions without the MATLAB-IDE.
        * The path-variable cwd specifies the path of the matlab-folder. Here, the path is copied from the definition in sim-config, but it can also be hardcoded here.
        * The no mat sessions should be equal to the number of MATLAB-simulators in this simulation.

If there are already suficient connectable MATLAB-sessions active, the script simply reconnects
to those.

How can I test and debug my model?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When first creating a simulator and a model, I recommend testing the functions for proper
input and output formats using either MATLABs own debug-modus or a simple test-script in
MATLAB. When the simulator is already included in the mosaik main-scirpt and an error occurs
within the MATLAB-code, it can be useful to include a simple keyboard-command just before
the faulty code-section. Pleasantly, mosaik will pause its simulation and MATLAB will go into
debug-mode so that the simulation can be debugged as if it were an all-MATLAB-program. If
there is a problem with the communication through java that cannot be resolved or found by
simple debugging in MATLAB, you might consider writing a testing-script in java. An example
for such a script is given in Appendix C.


