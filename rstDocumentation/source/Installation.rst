============
Installation
============

This section guides through the installation procedure.

Prerequisites
=============

The following programs should be installed on the computer:

        * **MATLAB**        I use MATLAB 2013b. No additional toolboxes are required.
        * **Python**        I use Python 3.4.2 (64-bit)
        * **Java**          I use Java 8 Update 66 and Java SE Development Kit 8 Update 60 (64-bit)

The installation procedure of these programs is well documented and can be found on the respective websites. If you wish to make any modifications to the applications, you might also want to install IDEs for this purpose. I used Eclipse Java Mars for Java and Pycharm Community Edition 4.5.

Installing mosaik
=================
As this is an API for mosaik, mosaik has to be set up before we can proceed to installing the
MATLAB-API. I recommend following the installation guide at https://mosaik.readthedocs.org.

The MATLAB-API
==============
To include the MATLAB-API, select a location for your MATLAB-directory.
The path of the MATLAB-directory must be set in the cwd-variable in the main python script in the ``sim_config`` variable.
For a MATLAB-simulator to work, the .m-files of the simulator and all models must simply be located in this directory or any sub-directory of it.
This directory should contain all of the following files:

        1. MATLAB Toolbox: jsonlab v1.2 (modified to handle containers.map)
        2. Java Archive: matlabcontrol-4.1.0.jar
        3. Java Archive: Matsim.jar
        4. cmd-file: StartMatlab.cmd
        5. Java class file: StartMatlab.class
        6. Matlab (abstract) class file: Simulator.m
        7. Matlab (abstract) class file: model.m
        8. Matlab class files: The inheriting simulator and model files of the realized simulator.
           All matlab-files can also be moved to sub-directories if so desired.

