====================
Relevant Class Files
====================
This section introduces the newly created classes that make up the API.
When using this API to implement a simulator in mosaik, only the MATLAB-part should be of importance, because the interaction with Java is part of the API and should need no further modifications.

MATLAB
=======

Simulator
---------
This is the abstract simulator class that connects to mosaik. It is called by the Java class MatlabSimulator over matlabcontrol. To implement a simulator, create a non-abstract simulator class inheriting from this class.  
Please feel free to use the irregularly indented debugging outputs (by commenting/uncommenting). 

Simulator class properties:
^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

        properties (SetAccess = protected)
           simName
           SimID
           metastruct = struct('models',struct(),'api version',2);
           initiated = false;
           step size
           entities = struct();

Most of these variables should be quite self-explanatory. simName specifies the simulator name, SimID is the simulator ID as assigned by mosaik, initiatied is a boolean specifying if the simulator is initiated, theapi_version is required by mosaik to be set to 2 and the step_size is the default step size in seconds. Furthermore, the entities struct is initialized in the constructor and will later (after the first create-call) hold all associated simulator model instances identified by their entity ID Eid.
The metastruct is equally important.
It should be set in the constructor of a simulator and will be returned to mosaik as the return value of the init method.

Simulator class methods:
^^^^^^^^^^^^^^^^^^^^^^^^
::

        methods (SetAccess = public)
           obj = Simulator(simName)
           metadata = init(obj, json sim params, sid)
           created = create(obj,number, model type, json model params)
           return json = step(obj,current time, json inputs, iteration)
           Json Answer = getData(obj,requested)
           delete(obj)

Here I describe the method functionality of the abstract simulator class. All these methods
can, if necessary, be overwritten or modified in any inheriting methods. However for most
simulators, simply setting the correct metastruct in the constructor will be the only necessary
changes when inheriting from this class.

        1. The constructor
                  takes simName as input string and saves it. For confenience, I often choose to define and set the metastruct in the constructor, although it is returned to mosaik from init.
        2. metadata = init(obj, json sim params, sid)
                   Initializes the Simulator using json sim params and returns the metadata metastruct-parameter. The metastruct is very important for the later integration into mosaik. This parameter has to contain the information how to initiate the simulator (via the metastruct.params) and which parameters are available to mosaik during runtime (via the metastruct.attrs). Mosaik can never access any parameters that are not specified in the metastruct.attrs parameter.

        3. created = create(obj,number, model type, json model params)
                   Creates number of instances of the simulator model model tope using the model params for all these instances. Returns the created instances as a struct(). The Entity IDs Eid are numbered continuously for every model, even if some entities of the requested model already exist. json model params should be a Json-object containing the initialization parameters for the models.
        4. recall time = step(obj, current time, json inputs, iteration)
                   This is the method that calls all connected simulator models in each step. json inputs should be a JSONObject mapping all the simulator models (by their Eids) to their specific input parameters. Only those models (i.e. their step methods) that receive at least one such input will be invoked each step. Optionally, json inputs may also contain a parameter step aize. In that case, this specific step will be executed with a difierent step sime than that which is specified as a simulator property. iteration is only needed if some models make a getData-request that they want to have the answer to before the next step. iteration starts out at 1. If any model of the simulator set his model.call me again to true, those models will be called a second time in the same step from java with iteration = 2.
        5. Json answer = getData(obj,requested)
                   This method is invoked when another simulator or model makes a getData-request to this model. requested should be a JSONObject mapping Model-IDs of Requested Entities to the names of their Requested fields. Returns Json Answer is a simple json Object just like requested, but mapping values to the requested fields.


model
-----

This is the abstract MATLAB class for mosaik simulator models used with the MATLAB-API
over Java. The abstract model sets the generally needed properties for any simulator model.
Furthermore, it provides a method for the simple unraveling of mosaiks json-formatted input
parameters and for the simple and standardized construction of asynchronous requests to other
models.

model class properties:
^^^^^^^^^^^^^^^^^^^^^^^
:: 

        properties (SetAccess = public)
        started
        Eid
        Simulator
        call me again

Every Simulator model should have the properties started (boolean), Eid (ID as valid for
mosaik), type (as specified in the *parent-simulators* metastruct) and call me again (boolean
for multiple calls in one step(). Only necessary in special situations). They should be set within
the constructor. (Consider using ``obj@model(Eid,params);`` for convenience in your inheriting
classes constructor.)

model class methods:
^^^^^^^^^^^^^^^^^^^^
::

        methods (SetAccess = public)
           asyncs = step(thisModel, inputs, step size, iteration)

        methods (SetAccess = protected)
           [entitynames absender, input values] = ...
                unravel inputs(thisModel, inputs, input parameter)
           asyncs = createAsyncRequest(thisModel,asyncs, type,...
                 fullTargetID, targetProperty, varargin)

.

        1. recall time = step(obj,time, step size, inputs)
                   This function is the core of the simulator model. Override this function to describe the behavior of each model during the simulation steps. If your model has any asynchronous requests to submit, construct them using the createAsyncRequest-method and return them in the asyncs-parameter. If no asynchronous requests are desired, leave it empty with asyncs = containers.Map(); Normally, the asynchronous requests will be executed after the step() of this simulator returns. If you are submitting a getData-request for which you need the answer within the same step(), you need to make use of the iteration-parameter as described in section 5.8

        2. [entitynames absender, input values] = unravel inputs(thisModel, inputs, input parameter)
                   This function can be used to get the values for a single input parameter from the inputs Map. entityname absender is a cell array with the entity IDs of all connected entities that sent a value for this input parameter. If the input parameter was not found, input values returns as NaN. If exactly one input was found, input values will be that value. If several were found, input values will be a cell array.
        3. asyncs = createAsyncRequest(thisModel,asyncs, type, fullTargetID, targetProperty, targetValue)
                   Creates an asynchronous request to mosaik that will be answered between this and the next iteration of the step() function. ``asyncs`` is the list of map requests (can contain the map containers ``getRequests`` und ``getRequests``). type is a string, either ``get`` or ``set``. ``fullTargetID`` is the full ID (as a string) of the model the request addresses. It usually takes the form: Sim ID.Model ID targetProperty is the Property (string) to be requested or modified targetValue is an optional parameter that is only used if type == ``set``. It specifies the new value for the target Property.


Java
====

In this section I present the Java classes which expand the `mosaik Java API`_  in order to form the java-side of the MATLAB-API.
Therefore, this section serves only to better understand the underlying processes. When using the MATLAB-API, no modifications in JAVA should be necessary at all.

.. _mosaik Java API: https://bitbucket.org/mosaik/mosaik-api-java

MatlabSimulator
---------------
This is a Java class inheriting from the Java-APIs Simulator-class and calls the MATLAB-
Simulator-class.
This class basically translates the Java-API methods to the respective MATLAB-API methods.
Its class properties and methods are listed and briefly commented on in the following tables.

Properties:

=============   ======================= =================================================
Property Name   Type                    Comment
=============   ======================= =================================================
matlab          private MatlabProxy     matlabcontrol proxy used to communicate to matlab
simName         pricate String          Simulator name string
meta            private JSONObject      The simulators ``meta`` information.
                                        Communicates the initialization parameters,
                                        available models and parameters.
=============   ======================= =================================================

Methods:

===============   ======================= ============================= =================================================
Method Name       Return Type             Parameters                    Comment
===============   ======================= ============================= =================================================
MatlabSimulator   MatlabSimulator         String ``simName``            Constrtuctor, also connects to MATLAB
init              JSONObject              String ``sid``                calls the initializion and returns ``meta``
                                          JSONObject ``simParams``
create            JSONArray<JSONObject>   Int ``num``                   creates ``num`` models using ``modelParams`` and
                                          String ``model``              returns a list of all created objects.
                                          JSONObject ``modelparams`` 
step              double                  double ``time``
                                          JSONObject ``inputs``         Calls step function and hadles *asyncronous requests*.
                                                                        Careful: matlabcontrol cannot transmit the *long* type. 
getData           JSONObject              JSONObject ``requests``       Retrieves the values of the parameters in ``requests``
                                                                        from the model.
Cleanup           ..                      ..                            Destructor, Disconnects from MATLAB.
===============   ======================= ============================= =================================================


**Sidenote - variable types:**

A careful consideration of the code will show that this class often deals with JSONObjects as a Map<String,Object> and JSONArrays as List<>. This is inherited from the Java-API Simulator base class and seems to work fine, even though the resulting conversion warnings have to be suppressed. Here, we referred to them as JSONObjects/Arrays.
