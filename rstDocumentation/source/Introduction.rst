============
Introduction
============

This documentation describes an application interface (API) for MATLAB-based simulator
models into the python-based mosaik environment.
We chose an approach that uses the existing interface from mosaik to Java to utilize the Java-
toolbox matlabcontrol in order communicate with MATLAB.
The realization of the MATLAB-API translates directly from the Java-API and can be utilized
in a nearly identical fashion.
Furthermore, all program-components aside from MATLAB itself are freely available.
In the figure below, the general dataflow within the MATLAB-API is illustrated. With this documentation,
I hope to provide a guideline to easily understand the API and its usage.


.. figure:: /MatAPIdocstatic/Dataflow_normal.jpg
    :width: 800
    :align: center
    :alt: Step call to the MATLAB API
    
    Illustration of a single step()-call from mosaik to a MATLAB-based simulator.
