.. MatAPIdocumentation documentation master file, created by
   sphinx-quickstart on Fri Jul 29 16:20:28 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MatAPIdocumentation's documentation!
===============================================

Contents:

.. toctree::
   :maxdepth: 2

   Introduction
   Components
   Installation
   Classes
   Step-by-Step
   Examples
   Errors
   Debugging


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

