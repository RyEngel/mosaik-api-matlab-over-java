Example Scenarios
*****************

Example1:  Electricity-meter-Simulator
======================================

This is a minimal version of a functional simulator.
It was integrated into the mosaik-demo program and has only one model that simply sums up the electricity that was consumed by each connected entity.

Stromzaehler
------------
model
^^^^^

Apart from setting properties and constructor, this model for an electricity-meter only modifies the step method.
The step method takes the current load of the connected entity and multiplies the value with the step-size to get the energy that the entity used in this time-step. Then it adds this value to the so far used energy. As there are no asynchronous requests made, the asyncs-map remains empty. ::

        classdef Stromzaehler < model
            %Stromzaehler 
            %   Simulator model for an electricity meter that integrates the used
            %   electric power of a connected entity over time.
            properties
                Energy %Integrated Energy
                Power %Current Power
                conn_entity
            end
            
            methods
                function obj = Stromzaehler(Eid, params, SimID)
                    obj@model(Eid, params, SimID);
                    obj.Energy = params.init_val;
                    obj.type = 'Stromzaehler';
                    obj.Power = 0;

                end
                
                function asyncs = step(obj, inputs, step_size, ~)
                    %fprintf('Step function of %s called \n', obj.Eid)
                    asyncs = containers.Map();

                        
                    try 
                        [~, Power] = obj.unravel_inputs(inputs, 'Power'); %check inputs for errors
                    catch
                        warning(['No Power-Input recieved by ' obj.Eid])
                        return
                    end
                                 
                    %actual calculation
                    obj.Power = Power;
                    obj.Energy = obj.Energy+(obj.Power*step_size);
                    %fprintf('Power at call end: %d \n', obj.Power)
                end
            end
        end



Simulator
^^^^^^^^^
As this is a very simple model, no modifications to the methods defined in the abstract Simulator class are necessary.
Therefore, this simulator needs only to define the metastruct parameter. ::

        classdef Zaehlersimulator < Simulator
            %Zaehlersimulator 
            % Matlab simulator for the "Stromzaehler"-models.
            
            properties
                
            end
            
            methods
                    %Concrete class Constructor
                    function obj = Zaehlersimulator(simName)
                    obj@Simulator(simName);
                    %obj@Simulator(simName);

                    zaehler_meta = struct(); %Define Metadata for counter-model
                    zaehler_meta.public = true;
                    zaehler_meta.params = {'init_val'};
                    zaehler_meta.attrs = {'started','conn_entity','Power','type','Eid','Energy'};
                    zaehler_meta.any_inputs = true;
                    
                    obj.metastruct.models = struct('Stromzaehler',zaehler_meta);
                          
                    end

            end
            
        end



Integration into demo.py
------------------------
To avoid errors during the opening of new matlab-sessions, I start MATLAB by 
modifying the python main() function as shown below. ::

        if __name__ == '__main__':
            no_mat_sessions = 1 #number of MATLAB sessions needed (= number of matlab simulators)
            debug_matlab = False #open matlab hidden (for quick running) or openly (for debugging)
            cwd = "C:\\Users\\rEngel\\Documents\\matlab-elements"  #matlab folder path
            if(debug_matlab):
                debug_param = "debug"
            else:
                debug_param = "nodebug"

            #calls the Script that calls the java class that preopens MATLAB, then waits for initialization to complete
            call(cwd +"\\StartMatlab" + " " + cwd + " " + str(no_mat_sessions) + " " + debug_param, shell = True)
            print('Waiting for MATLAB to get ready')
            time.sleep(2)

            main()


Where StartMatlab.cmd is a cmd-file starting or reactivating one matlab-session for each simulator that uses the MATLAB API. The *cwd* parameter is the MATLAB-directory as specified in *sim_config*.

The simulator must, as any mosaik-simulator, be specified in the *sim_config* variable.
A correct definition for theZaehlersimulator simulator would be: ::

        sim_config = {
        #	... other simulators ...
            'MATZSimulator': {
                'cmd': 'java -jar MatSim.jar %(addr)s Zaehlersimulator',
                'cwd': 'H:\\Official-API\\matlab-elements'
            },
        }


The *cwd* parameter sets the working directory for matlab. The *cmd* is the command line that starts the MATLAB-API, passing the socket address and the name of the desired MATLAB-simulator as arguments.


After this, the MATLAB-based simulator can be handled like any other simulator.
In our example, we chose to perform the following actions in the create_scenario function: ::

        def create_scenario(world):
            # Start simulators
        #   ...starting other simulators...

            MatZSimulator = world.start('MATZSimulator', step_size=60)

            # Instantiate models
        #	...instantiating other models...
            stromzaehler = MatZSimulator.Stromzaehler.create(houses.__len__(), init_val = 0) #Creating a Stromzaehler for each house

            # Connect entities
        #	...connecting other stuff...
            connect_zaehler_to_houses(world, houses, stromzaehler) #connect each counter to a house

            # Database
        #   ...setting up database...

            # Web visualization
        #    ...setting up simulators for WebVis....

            connect_many_to_one(world, stromzaehler, vis_topo, 'Energy')
            webvis.set_etypes({
                'Stromzaehler': {
                    'cls': 'load',
                    'attr': 'Energy',
                    'unit': 'E [J]',
                    'default': 0,
                    'min': 0,
                    'max': 1000000000,
                },
            })
        def connect_zaehler_to_houses(world, houses, stromzaehler):
            for n in range(0, houses.__len__()):
                world.connect(houses[n], stromzaehler[n], ('P_out', 'Power'))


Note that for the web visualization to work, the *etype* specified for the model (here *Stromzaehler*) must correspond to the *type* property specified in the model class constructor and simulator metastruct.

Example 2: Generator, Battery and Fuel-cell
===========================================

In this scenario, the battery communicates with a
generator and a FuelCell. It receives as input a
power-offer from both of them and charges/discharges
for the sum of those powers as much as it can. 
It then returns via setData how much of the Power it could
use. 
(However, the Generator model ignores this information.The FuelCell notices how much power was used and only delivers an according amount of power.)
The ChargeControl model is "stupid" and only knows on/off.
I set its "may_produce" parameter via setData.
For demonstration, I also included a getData call here. (The requestet value *SoC* could also be transmitted vie the regular inputs, i.e. mosaiks connect-mechanism.)
We rule that the FuelCell should only be charged if the SoC of the battery is more than 20% smaller than that of the FuelCell.
Otherwise, the battery shall charge the Fuelcell.

Because the Battery requests the *SoC* of the FuelCell 
via getData and wants the answer in the same step, we need to use the \texttt{switch iteration}-procedure in the step-function.
The async call getData will be created in the first iteration, executed from java between iterations, and the answer can be used in iteration two.

.. figure:: /MatAPIdocstatic/Demoschema.jpg
    :width: 800
    :align: center
    :alt: Dataflow between models in this example-scenario
    
    A simple illustration of the dataflow between the models of this Scenario.

The following pages list the code for the demo models and simulators.

Generator
---------
Model
^^^^^
::

        classdef Generator < model
            %   Random power generator model
            properties
                conn_entity
                power_out
                may_produce
                max_power

            end
            
            methods
                function gen = Generator(Eid, params, SimID)
                    gen@model(Eid, params, SimID);
                    gen.type = 'Generator';
                    gen.power_out = 0;
                    gen.may_produce = params.may_produce;
                    gen.max_power = params.max_power;
                end
                
                function asyncs = step(gen, inputs, ~, ~)
                    asyncs = containers.Map();
                    %% read inputs

                    [~, may_produce] = gen.unravel_inputs(inputs, 'may_produce'); %check inputs for errors

                    %fprintf('I got %d W. \n',input_power)
                    
                    if sum(may_produce==[0,1])
                        gen.may_produce = may_produce;
                    end

                    %% perform step
                    if gen.may_produce
                        gen.power_out = rand*gen.max_power;
                        %CC.power_out = abs(input_power)*0.1; %lets through 10% of the total power
                    else 
                        gen.power_out = 0;
                    end
        %fprintf('I produced %d W. \n',gen.power_out)
                end
            end
        end

Simulator
^^^^^^^^^
::

        classdef RPGSim < Simulator
            %FCSimulator: This is a Simulator for a dummy-fuelcell with very limited
            %functionality
            
            properties
            end
            
            methods
                    %Concrete class Constructor
                    function RPG = RPGSim(simName)
                    RPG@Simulator(simName);
                    %obj@Simulator(simName);

                    
                    RPG_meta = struct(); %Define Metadata for model
                    RPG_meta.public = true;
                    RPG_meta.params = {'max_power','may_produce'};
                    RPG_meta.attrs = {'power_out','type','Eid','input_power','may_produce'};
                    RPG_meta.any_inputs = true;
                    
                    RPG.metastruct.models = struct('Generator',RPG_meta);
                          
                    end
            end
        end



FuelCell
--------

Model
^^^^^
Fuelcell Model Sourececode ::

        classdef FuelCell < model
            properties
                offered_power %Current offered_power
                conn_entity
                tank
                tank_size
                max_power
                produce_or_consume
                efficiency
                SoC
                accepted_power
            end

            methods
                function FC = FuelCell(Eid, params, SimID)
                    FC@model(Eid, params, SimID);
                    FC.tank = params.tank_initial;
                    FC.tank_size = params.tank_size;
                    FC.max_power = params.max_power;
                    FC.type = 'FuelCell';
                    FC.offered_power = 0;
                    FC.produce_or_consume = 'consume';
                    FC.SoC = FC.tank/FC.tank_size;
                    FC.accepted_power = 0;
                    
                    %disp(FC)
                end
                
                function asyncs = step(FC, inputs, step_size, ~)
                    %keyboard
                    %% asyncs = step(inputs, step_size, iteration)
                    % The fuelcell is connected to the Battery so that its produced
                    % offered_power goes to the battery via:
                    % world.connect(fuelcells[0],batteries[0], ('offered_power', 'P_set'), async_requests=True)
                    % For demonstration, it requests the current State of Charge
                    % from the battery. If the battery is more than 80% full, it
                    % will start electrolysis and refill its tank, discharging the
                    % bat
                    tery
                    
                    %disp(FC)

                    asyncs = containers.Map();
                    
                    %% Read inputs
                    [~, accepted_power] = FC.unravel_inputs(inputs, 'accepted_power');

                    if ~isnan(accepted_power)
                        FC.accepted_power = accepted_power; %Save if input is valid
                    end
                    
                    [~, produce_or_consume] = FC.unravel_inputs(inputs, 'produce_or_consume');
                    
                    if isstr(produce_or_consume)
                        FC.produce_or_consume = produce_or_consume;
                    end
                    
                    %% Perform step()
                    % Adjust tank level depending on how much of the "offered"
                    % offered_power was accepted last turn
                    FC.tank = FC.tank - step_size * FC.accepted_power;  
                    
                    if strcmp(FC.produce_or_consume,'produce')
                        max_i_can_deliver = FC.tank/step_size;
                        FC.offered_power = min(FC.max_power, max_i_can_deliver);
                    elseif strcmp(FC.produce_or_consume,'consume')
                        max_i_can_consume = (FC.tank_size-FC.tank)/step_size;
                        FC.offered_power = -min(FC.max_power, max_i_can_consume);
                    end
                  
                    FC.SoC = FC.tank/FC.tank_size; %update State of Charge 
                    %fprintf('FC SoC: %d \n',FC.SoC)
                end
            end
        end

Simulator
^^^^^^^^^
Simulator Sourcecode ::

        classdef FCSimulator < Simulator
            %FCSimulator: This is a Simulator for a dummy-fuelcell with very limited
            %functionality
            
            properties
            end
            
            methods
                    %Concrete class Constructor
                    function FCsim = FCSimulator(simName)
                    FCsim@Simulator(simName);

                    FC_meta = struct(); %Define Metadata for model
                    FC_meta.public = true;
                    FC_meta.params = {'max_power','tank_size','tank_initial'};
                    FC_meta.attrs = {'started','offered_power','type','Eid','tank','input_power','produce_or_consume','SoC','accepted_power'};
                    FC_meta.any_inputs = true;

                    FCsim.metastruct.models = struct('FuelCell',FC_meta);
                          
                    end
            end
        end


Battery
-------
Model
^^^^^
Battery Model Sourcecode ::

        classdef Battery < model
            % Very basic battery model, to demonstrate mosaik interfacace.
            % To demonstrate how user defined objects can be integrated into the
            % model, the battery model consists of several batterycells, which are
            % entities of the user defined class BatteryCell. The user defined
            % classes Battery and BatteryCell provide methods to translate their
            % actual state into a struct, which is platform independent and which
            % can be send to mosaik. Mosaik starts a new matlab process and with
            % the state information in the struct it can instantiate a new battery
            % object in the new process, which is an exact copy of the original
            % battery. 
            
            properties (SetAccess = private)
                age; % battery age [sec]
                E_max; % max energy content of battery [J]
                E;      % Current stored energy
                P_get;  % real average power of battery [W]
                        % positive values: charge battery,
                        % netative values: discharge battery     
                P_set;  % desired average power of battery in next time step [W], 
                        % positive values: charge battery,
                        % netative values: discharge battery
                SoC;
                       
            end
            
            properties (Access = public)
                conn_entity;
            end
            
            properties (SetAccess = immutable)       
                batterycells;  % battery consists of several cells [1xn BatteryCell]
            end
            
            methods
                function battery = Battery(Eid, battery_params, SimID)  % constructor
                    battery@model(Eid, params, SimID);
                    battery.type = 'Battery';

                    % set initial battery age, if not specified 
                    % set default zero
                    if isfield(battery_params, 'age')
                        battery.age = battery_params.age;
                    else
                        battery.age = 0;  % default value
                    end
                    
                    battery.Eid = Eid;

                    % instantiate cells
                    % battery_params.batterycell = [1xn struct]
                    battery.batterycells = BatteryCell.empty;  % [0x0 BatteryCell]
                    for cell_params = battery_params.batterycells
                        battery.batterycells(end+1) = BatteryCell(cell_params);
                    end

                    % calculate E_max of battery as sum of cells E_max
                    battery.E_max = 0;
                    for batterycell = battery.batterycells
                        battery.E_max = battery.E_max + batterycell.E_max;
                    end

                    
                    %Get current total stored energy
                    [battery.SoC,battery.E] = get_charge_level(battery);
                end
                
                function asyncs = step(battery, inputs ,step_size, iteration)
                    %% scenario-specific step dunction
                    % In this scenario, the battery communicates with a
                    % ChargeControlSim and a FuelCell. It recieves an input
                    % power-offer  from both of them and charges/discharges
                    % for the sum of those powers as much as it can. 
                    % It then returns via setData how much of the Power it could
                    % use.
                    % 
                    % Scenario-funtionality:
                    % For demonstration, I show two different 
                    % control-machanisms.
                    % The FuelCell notices how much power was used and fits its
                    % behavior to this information.
                    % The ChargeControl model is "stupid" and only knows on/of.
                    % We set its "may_produce" parameter via setData
                    %
                    % From the FuelCell should only be charged if the SoC of the
                    % battery is more than 20% smaller than that of the fuelCell.
                    % Otherwise, the battery shall charge the fuelcell.
                    %
                    % getData-demonstration:
                    % Because the Battery requests the SoC of the fuelCell 
                    % via getData and wants the answer in the same step,
                    % we need to use the switch iteration in the step-function.
                    % The async call getData will be created in the first
                    % iteration, executed from java between iterations, and the
                    % answer can be used in iteration two.
                    asyncs = containers.Map();
                    
                    switch iteration
                        case 1                   
                            %% handling the async-requests
                            
                            % just to know the connected entities
                            [entitynames_absender, ~] = battery.unravel_inputs(inputs, 'P_set');


                            
                            % Decide if more charging from the CC is necessary
                            if get_charge_level(battery)<0.95 
                                charge_more = true;
                            else
                                charge_more = false;
                            end
                            % find the address of the CCs from the inputs
                            for address = entitynames_absender 

                                if (~isempty(strfind(address{1},'ChargeControlSim')) || ~isempty(strfind(address{1},'RPGSim')))
                                    %creating a may_produce call to the ChargeControl
                                    asyncs = createAsyncRequest(battery, asyncs, 'set', address{1}, 'may_produce', charge_more);
                                end
                            end
                            
                            % find the address of the FuelCell from the inputs
                            for address = entitynames_absender 
                                if logical(strfind(address{1},'FCSimulator'))
                                    %creating a getData request for SoC from the
                                    %FuelCellSim
                                    asyncs = createAsyncRequest(battery, asyncs, 'get', address{1}, 'SoC');
                                    
                                    battery.call_me_again = true; %step() is not done yet. Call again!
                                end
                            end

                        case 2

                            % read the inputs
                            [asyncs_absender, AsyncAnswers] = battery.unravel_inputs(inputs, 'AsyncAnswers'); %Read asnc Answers
                            [power_offerings_absender, offered_power] = battery.unravel_inputs(inputs, 'P_set'); %Read P_set -> offered power
                            FC_SoC = AsyncAnswers('SoC');
                            
                            %This is just to remember who offered how much power
                            %was supplied by which address in order to accept the
                            %power in the same proportions
                            partof_offered_power(1)= offered_power{1}/sum([offered_power{:}]); 
                            partof_offered_power(2)= offered_power{2}/sum([offered_power{:}]);
                            partof_offered_power(isnan(partof_offered_power))=0;
                            offered_power = sum([offered_power{:}]);

                            %decide if the FC should produce or consume on its next
                            %call. 
                            if battery.SoC < FC_SoC
                                %FC produces only if its 20% higher charged than the battery
                                FC_mode = 'produce'; 
                            else
                                FC_mode = 'consume';
                            end
                            asyncs = createAsyncRequest(battery, asyncs, 'set', asyncs_absender{1}, 'produce_or_consume', FC_mode);

                            
                            %% Regular battery step.    
                            set_power_el(battery,-offered_power);
                            n_cells = length(battery.batterycells); % number of cells
                            P_set_cell = battery.P_set / n_cells;
                            % distribute power equally to cells
                            [battery.SoC,battery.E] = get_charge_level(battery);
                            battery.P_get = 0; %P_get becomes the accepted power
                            for batterycell = battery.batterycells % step all cells
                                battery.P_get = battery.P_get + ...
                                batterycell.step(P_set_cell, step_size); 
                            end
                            [battery.SoC,battery.E] = get_charge_level(battery);
                            %% setData commands for FuelCell and ChargeControl

                            asyncs = createAsyncRequest(battery, asyncs, 'set', power_offerings_absender{1}, 'accepted_power', battery.P_get*partof_offered_power(1));
                            asyncs = createAsyncRequest(battery, asyncs, 'set', power_offerings_absender{2}, 'accepted_power', battery.P_get*partof_offered_power(2));

                            battery.call_me_again = false; %Done with step()
                    end

                    
                    
                    %update total current stored energy 
                    [battery.SoC,battery.E] = get_charge_level(battery);
        %fprintf('Battery charge level: %d \n',get_charge_level(battery));

                end
                
                function set_power_el(battery, P_el)
                    % sets the desired average power for the next time step [Watt]
                    % postive value: discharge battery
                    % negative value: charge battery
                    battery.P_set = -P_el;
                    % Attention: Internally opposite sign convention
                end
                    
                function P_el = get_power_el(battery)
                    % returns the actual power of the last time step [Watt]
                    % postive value: battery discharging
                    % negative value: battery charging
                    P_el = -battery.P_get;
                    % Attention: Internally opposite sign convention
                end
                
                function [L,E] = get_charge_level(battery)
                    % returns the actual charging state
                    % 0: empty, 1: fully charged
                    E = 0;
                    for batterycell = battery.batterycells
                        E = E + batterycell.E;
                    end
                    L = E / battery.E_max;
                end
                
                function P_max = get_P_el_max(battery)
                    % returns maximum power of battery
                    P_max = 0;
                    for batterycell = battery.batterycells
                        P_max = P_max - batterycell.P_min; 
                        % P_min and minus because of reversed internal sign convention
                    end
                end
                   
                function P_min = get_P_el_min(battery)
                    % returns minimum power of battery
                    P_min = 0;
                    for batterycell = battery.batterycells
                        P_min = P_min - batterycell.P_max; 
                        % P_max and minus because of reversed internal sign convention
                    end
                end
                    
                function battery_state = get_state(battery)
                    % translates battery object to struct
                    % -> platform independent representation of Battery object
                    % which is needed by mosaik to instantiate copies of Battery
                    % objects in a new matlab process
                    % Battery(battery.get_state()) delivers copy of
                    % battery
                    len = length(battery.batterycells);
                    cell_states(len) = battery.batterycells(end).get_state();
                    % initialize struct array of appropriate size to take take 
                    % structs representing the cells
                    i = 1;
                    for batterycell = battery.batterycells(1:end-1);
                        cell_states(i) = batterycell.get_state();
                        i = i+1;
                    end
                    battery_state = struct( ...
                        'age', battery.age, ...
                        'batterycells', cell_states ...
                        );
                end
                
                function battery_copy = copy(battery)
                    % delivers exact copy of battery, which is completly indipendent
                    % from original battery and which is initilized with the actual
                    % state of the original battery
                    battery_copy = Battery(battery.get_state());
                end
            end
        end


Simulator
^^^^^^^^^
Simulator Sourcecode::

        classdef MatBatSim < Simulator
            %Battery Simulator class
            
            properties
                %entities = struct();
            end
            
            methods
        %% CONSTRUCTOR
                function obj = MatBatSim(simName)
                    obj@Simulator(simName);
                    
                    battery_meta = struct(); %Define Metadata for counter-model
                    battery_meta.public = true;
                    battery_meta.params = {'age','batterycells'}; %creation-params
                    battery_meta.attrs = {'conn_entity','type','Eid',...
                        'age', 'E_max', 'P_set', 'P_get', 'E', 'requested_power','SoC'}; % acessable attrs
                                    % 'E' gibt es nicht direkt als eigenschaft, muss aber für get_Data zugänglich sein
                    battery_meta.any_inputs = true;
                    
                    obj.metastruct.models = struct('Battery',battery_meta);
                end    
            end
        end

Python main script
------------------
This is the main mosaik script ::

        __author__ = 'rengel'
        import itertools
        import random
        import time
        import json
        from subprocess import call
        from mosaik.util import connect_randomly, connect_many_to_one
        import mosaik


        sim_config = {
            'CSV': {
                'python': 'mosaik_csv:CSV',
            },
            'WebVis': {
                'cmd': 'mosaik-web -s 0.0.0.0:8000 %(addr)s',
            },
            'MAT-FCSimulator': {
                'cmd': 'java -jar MatSim.jar %(addr)s FCSimulator',
                'cwd': 'H:\\mosaik-api-matlab-over-java\\MATLAB-API-DEMO\\matlab-elements'
            },
            'MAT-MatBatSim': {
                'cmd': 'java -jar MatSim.jar %(addr)s MatBatSim',
                'cwd': 'H:\\mosaik-api-matlab-over-java\\MATLAB-API-DEMO\\matlab-elements'
            },
            'MAT-RPGSim': {
                'cmd': 'java -jar MatSim.jar %(addr)s RPGSim',
                'cwd': 'H:\\mosaik-api-matlab-over-java\\MATLAB-API-DEMO\\matlab-elements'
            }
        }

        START = '2014-01-01 00:00:00'
        END = 1 * 3600*5 # simulation time in seconds


        with open("Battery_Params.json") as json_file:
            Battery_Params = json.load(json_file)
            json_file.closed

        def main():
            random.seed(23)
            world = mosaik.World(sim_config)
            create_scenario(world)
            world.run(until=END)  # As fast as possilbe
            # world.run(until=END, rt_factor=1/60)  # Real-time 1min -> 1sec


        def create_scenario(world):
            # Start simulators


            MatBatSim = world.start('MAT-MatBatSim', step_size=60)
            FCSim = world.start('MAT-FCSimulator', step_size=60)
            RPGSim = world.start('MAT-RPGSim', step_size=60)

            # Instantiate models

            batteries = MatBatSim.Battery.create(1, age = Battery_Params['age'],
                                                       batterycells = Battery_Params['batterycells'])
            fuelcells = FCSim.FuelCell.create(1, max_power = 15000, tank_size = 1e8, tank_initial = 1e8)
            generators = RPGSim.Generator.create(1, max_power = 10000, may_produce = 1)

            # Connect entities

            world.connect(fuelcells[0],batteries[0], ('offered_power', 'P_set'), async_requests=True)
            world.connect(generators[0],batteries[0], ('power_out', 'P_set'), async_requests=True)

            # Web visualization
            webvis = world.start('WebVis', start_date=START, step_size=60)
            webvis.set_config(ignore_types=['Topology', 'ResidentialLoads', 'Grid',
                                            'Database'])
            vis_topo = webvis.Topology()



            connect_many_to_one(world, fuelcells, vis_topo, 'tank')
            webvis.set_etypes({
                'FuelCell': {
                    'cls': 'gen',
                    'attr': 'tank',
                    'unit': 'E [J]',
                    'default': 0,
                    'min': 0,
                    'max': 1000000000,
                },
            })
            connect_many_to_one(world, batteries, vis_topo, 'E')
            webvis.set_etypes({
                'Battery': {
                    'cls': 'load',
                    'attr': 'E',
                    'unit': 'E [J]',
                    'default': 0,
                    'min': 0,
                    'max': 1000000000,
                },
            })
            connect_many_to_one(world, generators, vis_topo, 'power_out')
            webvis.set_etypes({
                'Generator': {
                    'cls': 'gen',
                    'attr': 'power_out',
                    'unit': '[W]',
                    'default': 0,
                    'min': 0,
                    'max': 1,
                },
            })

        if __name__ == '__main__':

            no_mat_sessions = 3 #number of MATLAB sessions needed (= number of matlab simulators)
            debug_matlab = True #open matlab hidden (for quick running) or openly (for debugging)
            cwd = sim_config.get('MAT-FCSimulator').get('cwd')  #matlab folder path
            if(debug_matlab):
                debug_param = "debug"
            else:
                debug_param = "nodebug"

            #calls the Script that calls the java class that preopens MATLAB, then waits for initialization to complete
            call(cwd +"\\StartMatlab" + " " + cwd + " " + str(no_mat_sessions) + " " + debug_param, shell = True)
            print('Waiting for MATLAB to get ready')
            time.sleep(2)

            main()
        }

