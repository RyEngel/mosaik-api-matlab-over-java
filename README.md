# README #

### What is this repository for? ###

* This is a MATLAB-api for mosaik that works as an extension to the existing mosaik-java-api.
* Version: 2.0
* A full documentation (pdf) is included in the files
* Includes a demo-version that bases on the mosaik-demo (all files included)
