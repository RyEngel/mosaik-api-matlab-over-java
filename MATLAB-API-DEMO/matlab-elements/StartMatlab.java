import matlabcontrol.*;
import java.util.List;
import java.util.ArrayList;

public class StartMatlab 
{

		public static void main(String[] args)
		throws MatlabConnectionException, MatlabInvocationException
	{
		// create proxy
		MatlabProxyFactoryOptions options = new MatlabProxyFactoryOptions.Builder()
		.setUsePreviouslyControlledSession(true).setHidden(true)
		.build();		
		MatlabProxyFactory factory = new MatlabProxyFactory(options);
		
		int sessions = 0;
		
		if(args.length ==0)
		{
			sessions = 1;
		}else{
			sessions = Integer.parseInt(args[0]);
		}
		
		
		List<MatlabProxy> SessionList = new ArrayList<MatlabProxy>();

		for(int k=0;k < sessions;k++)
		{
			SessionList.add(factory.getProxy());
			// call builtin function
			SessionList.get(k).eval("disp('Matlab Initiated from batch file.')");
			SessionList.get(k).eval("cd "+ System.getProperty("user.dir"));
			SessionList.get(k).eval("addpath(genpath('.'))");
		}
		
		
		// close connections
		
		for(int l=0;l < sessions;l++)
		{  
			SessionList.get(l).disconnect();
		}
	
	}
		
}
