%Testscript Fuelcell simulator
clear 
IamFCSim = FCSimulator('IamFCSim')
metadata = IamFCSim.init(savejson('',struct('step_size',60)), 'FCSID')
IamFCSim.create(1,'FuelCell',savejson('',struct('max_power', 5, 'tank_size', 100, 'tank_initial', 60)))
IamFCSim.entities.FuelCell_1.step(struct(),1)
