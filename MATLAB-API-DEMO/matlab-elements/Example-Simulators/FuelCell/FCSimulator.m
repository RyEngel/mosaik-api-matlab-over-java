classdef FCSimulator < Simulator
    %FCSimulator: This is a Simulator for a dummy-fuelcell with very limited
    %functionality
    
    properties
    end
    
    methods
            %Concrete class Constructor
            function FCsim = FCSimulator(simName)
            FCsim@Simulator(simName);

            FC_meta = struct(); %Define Metadata for model
            FC_meta.public = true;
            FC_meta.params = {'max_power','tank_size','tank_initial'};
            FC_meta.attrs = {'started','offered_power','type','Eid','tank','input_power','produce_or_consume','SoC','accepted_power'};
            FC_meta.any_inputs = true;

            FCsim.metastruct.models = struct('FuelCell',FC_meta);
                  
            end
    end
end

