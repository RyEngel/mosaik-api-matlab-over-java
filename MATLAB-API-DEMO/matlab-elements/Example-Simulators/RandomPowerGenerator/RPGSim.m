classdef RPGSim < Simulator
    %FCSimulator: This is a Simulator for a dummy-fuelcell with very limited
    %functionality
    
    properties
    end
    
    methods
            %Concrete class Constructor
            function RPG = RPGSim(simName)
            RPG@Simulator(simName);
            %obj@Simulator(simName);

            
            RPG_meta = struct(); %Define Metadata for model
            RPG_meta.public = true;
            RPG_meta.params = {'max_power','may_produce'};
            RPG_meta.attrs = {'power_out','type','Eid','input_power','may_produce'};
            RPG_meta.any_inputs = true;
            
            RPG.metastruct.models = struct('Generator',RPG_meta);
                  
            end
    end
end

