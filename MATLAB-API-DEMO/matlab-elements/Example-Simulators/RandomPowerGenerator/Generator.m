classdef Generator < model
    %   Random power generator model
    properties
        conn_entity
        power_out
        may_produce
        max_power

    end
    
    methods
        function gen = Generator(Eid, params, SimID)
            gen@model(Eid, params, SimID);
            gen.type = 'Generator';
            gen.power_out = 0;
            gen.may_produce = params.may_produce;
            gen.max_power = params.max_power;
        end
        
        function asyncs = step(gen, inputs, ~, ~)
            asyncs = containers.Map();
            %% read inputs

            [~, may_produce] = gen.unravel_inputs(inputs, 'may_produce'); %check inputs for errors

            %fprintf('I got %d W. \n',input_power)
            
            if sum(may_produce==[0,1])
                gen.may_produce = may_produce;
            end
            

            %% perform step
            if gen.may_produce
                gen.power_out = rand*gen.max_power;
                %CC.power_out = abs(input_power)*0.1; %lets through 10% of the total power
            else 
                gen.power_out = 0;
            end
%fprintf('I produced %d W. \n',gen.power_out)
        end
    end
end

