classdef ChargeControl < model
    %FuelCell 
    %   Simplified fuelcell model
    properties
        conn_entity
        power_out
        may_produce
        %input_power
    end
    
    methods
        function CC = ChargeControl(Eid, params, SimID)
            CC@model(Eid, params, SimID);
            CC.type = 'ChargeControl';
            CC.power_out = 0;
            CC.may_produce = params.may_produce;

            
            
            %disp(params)
        end
        
        function asyncs = step(CC, inputs, ~, ~)
            asyncs = containers.Map();
            %% read inputs

            [~, input_power] = CC.unravel_inputs(inputs, 'input_power'); %check inputs for errors
            [~, may_produce] = CC.unravel_inputs(inputs, 'may_produce'); %check inputs for errors
            
            %fprintf('I got %d W. \n',input_power)
            
            if sum(may_produce==[0,1])
                CC.may_produce = may_produce;
            end
            
            max_power = 10000;
            %% perform step
            if CC.may_produce
                CC.power_out = abs(input_power)*0.5; %lets through 10% of the total power
            else 
                CC.power_out = 0;
            end
            
            %fprintf('I let %d W through. \n',CC.power_out)
            CC.call_me_again = false;
        end


     
    end
    
end

