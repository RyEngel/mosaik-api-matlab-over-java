classdef Zaehlersimulator < Simulator_old
    %Zaehlersimulator 
    % Matlab simulator for the "Stromzaehler"-models.
    
    properties
        
    end
    
    methods
            %Concrete class Constructor
            function obj = Zaehlersimulator(simName)
            obj@Simulator_old(simName);
            %obj@Simulator(simName);

            zaehler_meta = struct(); %Define Metadata for counter-model
            zaehler_meta.public = true;
            zaehler_meta.params = {'init_val','empty'};
            zaehler_meta.attrs = {'started','conn_entity','Power','type','Eid','Energy'};
            zaehler_meta.any_inputs = true;
            
            obj.metastruct.models = struct('Stromzaehler',zaehler_meta);
                  
            end

    end
    
end
