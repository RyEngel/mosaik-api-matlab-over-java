clear
close all

%Zun�chst nur das model:


Systemsize = 3;  % kWh


% In diesem Fall nur 2 Zellen aktiv:
cell_params = [struct(...
    'E', 0e6,...                        %Start Energy
    'E_max', Systemsize*3600*1000,...   %Max Energy in Joule
    'P_max', Systemsize*1000 /2,...     %Max chargy power
    'P_min', -Systemsize*1000 /2) ...   %Max discharge power
    struct(...
    'E', 0e6,...                        %Start Energy
    'E_max', Systemsize*3600*1000,...   %Max Energy in Joule
    'P_max', Systemsize*1000 /2,...     %Max chargy power
    'P_min', -Systemsize*1000 /2) ];

battery_params = struct(...
    'age', 60*60*100,...            %current age (100h)
    'batterycells', cell_params);   %cells. See above




%create sim
BatSim = MatBatSim('BatSim');

%init sim
sim_params = struct();
sim_params.step_size = 1;
BatSim.init(savejson('',sim_params),'BatSimID');

%create models
BatSim.create(2,'Battery',savejson('',battery_params));

%step with both models
Entities = fieldnames(BatSim.entities);
RequestAll = struct();
RequestAll.(Entities{1}) = BatSim.metastruct.models.Battery.attrs;
RequestAll.(Entities{2}) = BatSim.metastruct.models.Battery.attrs;


Stepinput.(Entities{1}).requested_power = struct('source',-200); %negative = battery charging --> momentan andersrum zum testen
Stepinput.(Entities{2}).requested_power = struct('source',500);

time = BatSim.step(0,savejson('',Stepinput),1);
%answer = BatSim.getData(RequestAll);

Stepinput.(Entities{1}).requested_power = struct('source',100); %negative = battery charging
Stepinput.(Entities{2}).requested_power = struct('source',-500);

time = BatSim.step(time,savejson('',Stepinput),1);
%answer = BatSim.getData(RequestAll);

Stepinput.(Entities{1}).requested_power = struct('source',-200); %negative = battery charging
Stepinput.(Entities{2}).requested_power = struct('source',100);

time = BatSim.step(time,savejson('',Stepinput),1);
%answer = BatSim.getData(RequestAll);

Stepinput.(Entities{1}).requested_power = struct('source',300); %negative = battery charging
Stepinput.(Entities{2}).requested_power = struct('source',500);

time = BatSim.step(time,savejson('',Stepinput),1);
%answer = BatSim.getData(RequestAll);



