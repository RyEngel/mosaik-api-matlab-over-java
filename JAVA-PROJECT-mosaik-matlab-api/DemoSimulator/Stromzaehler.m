classdef Stromzaehler < model
    %Stromzaehler 
    %   Simulator model for an electricity meter that integrates the used
    %   electric power of a connected entity over time.
    properties
        Energy %Integrated Energy
        Power %Current Power
        conn_entity
    end
    
    methods
        function obj = Stromzaehler(Eid, params)
            obj@model(Eid,params);
            obj.Energy = params.init_val;
            obj.type = 'Stromzaehler';
            %disp(params)
        end
        
        
        function recall_time = step(obj,time, step_size, inputs)  
            %fprintf('Step function of %s called \n', obj.Eid)

                
            entitynames = fieldnames(inputs.Power);
            if ~(length(entitynames) == 1) %Check if connected to only one entity
                warning('Not connected to exactly 1 entity');
            else
                entityname = entitynames{1};
            end
            if exist(obj.conn_entity)&&~strcmp(obj.conn_entity, entityname)
                warning('Connected entity of counter %s changed.', obj.Eid)
            else
                obj.conn_entity = entityname;
            end
                                          
            %actual calculation
            obj.Power = getfield(inputs.Power,entityname);            
            obj.Energy = obj.Energy+(obj.Power*step_size);
            
            %Define return value
            recall_time = time + step_size;
           
            
%             if obj.Eid(end-1:end)=='_1' %DEBUG outputs
%                 fprintf('Old Energy: %d\n', obj.Energy-obj.Power*step_size)
%                 fprintf('Added Energy: %d\n', obj.Power*step_size)
%                 fprintf('New Energy(Stromzaehler_1): %d \n', obj.Energy);  
%             end


        end
     
    end
    
end

