import matlabcontrol.*;
//import matlabcontrol.extensions.*;

import java.util.*;
//import java.util.List;
//import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
//import org.json.simple.JSONException;

import de.offis.mosaik.api.MosaikProxy;
import de.offis.mosaik.api.SimProcess;
import de.offis.mosaik.api.Simulator;

public class MatlabSimulator extends Simulator{
	
	private MatlabProxy matlab;
	private String simName;
	@SuppressWarnings("unused")
	private static JSONObject meta = new JSONObject();
	//protected MosaikProxy mosaik;
	
//	@SuppressWarnings("unchecked")
    /**
     * Is directly beeing called by mosaik as specified in sim_config.
     * For example'java -jar MatSim.jar %(addr)s Zaehlersimulator'
     * @param args - args[0] is the Socket address, args[1] is the simulator
     * name
     */
	public static void main(String[] args) throws Throwable
	{
		System.out.print("Java: MATLAB-API starting on connection "+args[0]+"\n");
		
		Simulator bat = new MatlabSimulator(args[1]);	
		
		SimProcess.startSimulation(args, bat);
		
	}
	
    /**
     * Creates a new simulator instance.
     * Also creates the Connection to matlab.
     * @param simName is the simulation's name.
     */
    public MatlabSimulator(String simName) throws MatlabConnectionException, MatlabInvocationException
    {
        super(simName); //Invoke parent constructor
        this.simName = simName;
        //mosaik = this.getMosaik();
        
		// Create Matlab instance
		MatlabProxyFactoryOptions options = new MatlabProxyFactoryOptions.Builder()
				.setUsePreviouslyControlledSession(true).setHidden(true)
				.build();
		MatlabProxyFactory factory = new MatlabProxyFactory(options);
		this.matlab = factory.getProxy();
		
		
		// Go to current directory
		matlab.eval("cd "+ System.getProperty("user.dir"));
		// Invoke Matlab constructor
		this.matlab.eval("MATS"+simName+" = "+simName+"('MATS"+simName+"');");

		
		System.out.print("Java: Constructor Executed. New " +simName+" created in MATLAB.\n");
    }
    
    /**
     * Initialize the simulator with the ID <em>sid</em> and apply additional
     * parameters <em>(simParams)</em> sent by mosaik.
     *
     * @param sid is the ID mosaik has given to this simulator.
     * @param simParams a map with additional simulation parameters.
     * @return the meta data dictionary (see {@link
     *         https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#init}).
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	@Override
    public Map<String, Object> init(String sid, Map<String, Object>
            simParams) throws MatlabConnectionException, MatlabInvocationException
    {
//    	System.out.print("Java init function called.\n");
    	//Call Matlab function
    	Object[] meta_recieved = this.matlab.returningEval("MATS"+this.simName + ".init('"+simParams+"','"+ sid +"')",1);
    	
    	//Object[]->String->JSONValue->JSONObject
    	JSONObject meta = (JSONObject)JSONValue.parse((String)meta_recieved[0]);
    	MatlabSimulator.meta = meta;
    	
//    	System.out.print("Meta = \n"+meta+"\n");
//    	System.out.print("Java init function executed.\n");
    	return meta;
    }


    /**
     * Create <em>num</em> instances of <em>model</em> using the provided
     * <em>model_params</em>.
     *
     * @param num is the number of instances to create.
     * @param model is the name of the model to instantiate. It needs to be
     *              listed in the simulator's meta data and be public.
     * @param modelParams is a map containing additional model parameters.
     * @return a (nested) list of maps describing the created entities (model
     *         instances) (see {@link
     *         https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#create}).
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	@Override
    public  List<Map<String, Object>> create(int num, String model,
            Map<String, Object> modelParams) throws MatlabConnectionException, MatlabInvocationException
    {
//    	System.out.print("Java create function called.\n");
//    	System.out.print(modelParams+"\n");
    	
    	//Call Matlab function
    	Object[] created_recieved = this.matlab.returningEval("MATS"+this.simName + ".create("+num+",'"+model+"','"+modelParams+"')",1);
  
    	//Extract returned JSONArray
    	JSONArray created = (JSONArray)JSONValue.parse((String)created_recieved[0]);
    	
//    	System.out.print(created+"\n");
    	return created;
    }

    /**
     * Perform the next simulation step from time <em>time</em> using input
     * values from <em>inputs</em> and return the new simulation time (the time
     * at which <code>step()</code> should be called again).
     *
     * @param time is the current time in seconds from simulation start.
     * 			I could not find a way to retrieve the "long"-type from matlab. \R
     * @param inputs is a map of input values (see {@link
     *               https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#step}).
     * @return the time at which this method should be called again in seconds
     *         since simulation start. Only double precision actually given.
     * @throws Exception 
     */
    @SuppressWarnings("unchecked")
	public long step(long time, Map<String, Object> inputs)
            throws Exception
    { 
    						//System.out.print("Java step function called.\n");
    	
    	boolean foundrequest = false;
    	Map<String, Object> ThisModelInputs = new JSONObject();
    	HashMap<String, Object> InputsCopy = new JSONObject();
    	boolean call_again = true;
    	long new_time = -1;
    	long recieved_time = -1;
    	int iteration = 1;
    	//String requests = "";
    	while(call_again)
    	{
    						//System.out.print("Java Step loop iteration: "+iteration+".\n");
    						//System.out.print("Input is: "+inputs+"\n");
    		
    		
// ----  Call step a first time and unravel the returned answers  ---------
    	   	Object[] recieved = this.matlab.returningEval("MATS"+this.simName + ".step("+time+",'"+inputs+"',"+iteration+")",1);
    	   					//System.out.print("Recieved: "+recieved + "\n"); //something like: [Ljava.lang.Object;@29774679
    	    JSONObject answer = (JSONObject)JSONValue.parse((String)recieved[0]);
    	   	JSONObject return_struct = (JSONObject)answer.get("return_struct");
    	   					//System.out.print("Recieved: "+return_struct.keySet()+"\n");
	    	recieved_time = (long)return_struct.get("recall_time");
	    	if ((long)return_struct.get("call_sim_again") ==1){
	    		call_again = true;}
	    	else{
	    		call_again = false;
	    	}
	    	JSONObject Requests = (JSONObject)return_struct.get("SimRequests");
	    	if (Requests.isEmpty()){continue;}; // break this iteration if there are no requests
	    	   	
// ---- Prepare for Async Requests  (create mosaik proxy)  --------------
	    	
	    	MosaikProxy mosaik = this.getMosaik();
	    	Set<String> RequestingModels = Requests.keySet();

// ---- Request current progress -----------------------------------------
	    	
	    	float progress = mosaik.getProgress();
	    	System.out.println("Progress: " + progress);
	    	
// ---- Execute Async Requests -------------------------------------------
	    	
	    	//iterate through requesting models
	    	for (String Modelname : RequestingModels){
				Map<String,Object> Model = (JSONObject)Requests.get(Modelname);
				InputsCopy = (JSONObject)inputs;
				ThisModelInputs.putAll((JSONObject)InputsCopy.get(Modelname));
				List<Map<String, Object>> AsyncAnswers = new JSONArray();

				
// ---- get Requests -----------------------------------------------------
	    		if (Model.containsKey("getRequests")){
	    			Map<String,List<String>> getRequests = (JSONObject)Model.get("getRequests");
	    							//System.out.print("I found this getRequest: "+getRequests+"\n");
	    			
	    			//iterate through addressed simulators
	    			for(String Addressed_Entity_ID : getRequests.keySet()){ 
    					List<String> Addressed_Parameters = getRequests.get(Addressed_Entity_ID);
    								//	System.out.print("The Request asks this Model: "+Addressed_Modelname+"\n" + "\t for these data "+ Addressed_Model+"\n");
    					
    					Map<String,List<String>> this_request = new HashMap<String,List<String>>();
    					this_request.put(Addressed_Entity_ID,Addressed_Parameters);    					
    								//System.out.print("I requested: "+this_request+"\n");
    					
    					Map<String, Object> data = new HashMap<String,Object>();
    					try {
    						//
    						data = mosaik.getData(this_request); // <------ Actual request to mosaik
    						foundrequest = true;

    								//System.out.print("And received: "+data+"\n");
    								//System.out.print("Old Inputs was: "+inputs+"\n");

    						AsyncAnswers.add(data);
    								//System.out.print("ThisModelInputs: "+ThisModelInputs+"\n");
    						
    						
    						} catch (NullPointerException BadReturnFromMosaik) {
    						System.out.println("mosaik.getData returned null! ");
    						} finally {}
	    			}
	    			if(foundrequest){
	    				
	    			// Copy the Answers to the input Map
	    			ThisModelInputs.put("AsyncAnswers",AsyncAnswers);
					InputsCopy.put(Modelname, ThisModelInputs);
					inputs = InputsCopy;
	    			}

	    		}
	    		

// ---- set Requests ------------------------------------------------------
	    		// request correct structure (from readthedocs):
	    		// {'src_full_id': {'dest_full_id': {'attr1': 'val1', 'attr2': 'val2'}}}
	    		
	    		if (Model.containsKey("setRequests")){
	    			Map<String,Object> setRequests = (JSONObject)Model.get("setRequests");
								//System.out.print("I found this setRequest: "+setRequests+"\n");
									
						boolean request_executed = false;    						
		    			try {

		    				mosaik.setData(setRequests); // <------ Actual request to mosaik
		    				
		    				request_executed = true;    						
		    						
	    					} catch (NullPointerException BadReturnFromMosaik) {
	    					System.out.println("mosaik.setData returned null! ");
	    					} finally {}
					    	
					 
	    			
	    		}
	    		
	    		//assert (getRequests != null || setRequests != null); //This point should not be reached if there are no requests.

	    	}

	    	iteration ++;
    	}
    	new_time = recieved_time;
    	return new_time; 
    	
    }

    /**
     * Return the data for the requested attributes in *outputs*
     *
     * @param requests is a mapping of entity IDs to lists of attribute names.
     * @return a mapping of the same entity IDs to maps with attributes and
     *         their values (see {@link
     *         https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#get-data}).
     * @throws MatlabConnectionException, MatlabInvocationException
     */
    @SuppressWarnings("unchecked")
	public  Map<String, Object> getData(
            Map<String, List<String>> requests) throws MatlabConnectionException, MatlabInvocationException
    {
//    	System.out.print("Java getData called. \n");
    	//Call Matlab function
    	Object[] recieved = this.matlab.returningEval("MATS"+this.simName + ".getData('"+requests+"')",1);
    	
    	//Object[]->String->JSONValue->JSONObject
    	JSONObject answer = (JSONObject)JSONValue.parse((String)recieved[0]);
    	
//    	System.out.print("Java getData executed. \n");
    	return answer;	
    }
    /**
     * This method is executed just before the sim process stops.
     *
     * Disconnects from MATLAB
     *
     * @throws MatlabConnectionException, MatlabInvocationException
     */
    @Override
    public void cleanup() throws MatlabConnectionException, MatlabInvocationException
    {
    	//this.matlab.eval(this.simName+".delete()"); //Destructor call
    	//this.matlab.eval("clear "+this.simName); // Delete handle
		this.matlab.eval("clear");
    	this.matlab.disconnect();
		System.out.print("Disconnected from MATLAB. \n");
        return;
    }

		
} 
