function [ OutputStruct ] = myCell2Struct( InputCells )
%Cell to Struct cenversion: [ OutputStruct ] = myCell2Struct( InputCells )
%   Converts a 1xN Cell Array containing Struct with equal fields
%   to a 1xN Struct array containing the respective fields.

if ~ min(size(InputCells))==1
    error('At leasrt one dimension of the input cell array must be 1.')
end


for i = 1:length(InputCells)
    if ~isstruct(InputCells{i})
        error('CellArray contains non-struct-element')
    end
    
    if exist('OutputStruct','var')
        OutputStruct = [OutputStruct,InputCells{i}];
    else
        OutputStruct = InputCells{i};
    end
end



end

