%% model
% Abstract MATLAB class for a mosaik simulator model 
% used with the MATLAB-API over Java.

classdef (Abstract) model < handle
    %% Standart properties and constructor:
    % Every Simulator model should have the properties <started>,                                                                                                                                                                                                                                                                                                                                                                                          <Eid> and
    % <type>. They should set within the constructor. 
    % (Consider using 'obj@model(Eid,params);' for convenience in your constructor.)
    properties
        started
        Eid
        type
    end
    methods
        function obj = model(Eid, params)
            obj.Eid = Eid;
            obj.started = true;
        end
       
        
        function recall_time = step(obj,time, step_size, inputs)
        %% recall_time = step(obj,time, step_size, inputs)
        % This function is the core of the simulator model. Override this
        % function to describe the behaviour of each model during the
        % simulation steps. The function must always return the simulation
        % time <recall_time> at wich the model wants to be called next by
        % the simmanager.
        
        
            recall_time = time + step_size;
        end

    end
end     