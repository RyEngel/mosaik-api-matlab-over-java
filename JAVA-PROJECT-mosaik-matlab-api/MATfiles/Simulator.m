%% Simulator
% This is an abstract simulator class that serves as an API to mosaik.
% It should be called by the Java class MatlabSimulator over matlabcontrol.
% Create a non-abstract simulator class inheriting from this class. As a
% base class for simulator models, use the class 'model'.
% Please feel free to use (by commenting/uncommenting) the irregularly indented
% debugging outputs.

classdef (Abstract) Simulator < handle  %inheriting from handle class to make attribute changes persistent.
%% Standart properties
    properties (SetAccess = protected)
        simName
        metastruct = struct('models',struct(),'api_version',2);
        initiated = false;
        step_size
        entities = struct();
    end

%%    
    methods
%% Simulator Constructor
% Takes <simName> as input string
        function obj = Simulator(simName)
    	    disp(['Executing MATLAB Constructor for: ', simName])
                
            obj.simName = simName;
        end
        
        
%% metadata = init(obj, json_sim_params)
% Initializes the Simulator using <json_sim_params> and returns the
% <metadata>
        function metadata = init(obj, json_sim_params)
    disp('Matlabs Simulator.init method called.')        
    %global DEBUG
    %if DEBUG
    %    save('./DEBUG/init_workspace')
    %end        
            obj.initiated = true;         
            sim_params = loadjson(json_sim_params);
            
            if isfield(sim_params,'step_size')
                obj.step_size = sim_params.step_size;
            end
            
            metadata = savejson('', obj.metastruct); %convert metastruct to json metadata
        end
        
%% created = create(obj,number, model_type, json_model_params)
% Creates <number> instances of the simulator model <model_type>
% using the <model_params> for all these instances. Returns the
% created instances as a sturct() called Entities.
% The Entity IDs <Eid> are numbered continously for every model, even if
% some entities of the requested model already exist.
% <model_params> should be a Json object Containing the initialization
% parameters for the models.
        function created = create(obj,number, model_type, json_model_params)
    disp('Matlabs Simulator.create method called.')
    global DEBUG
    if DEBUG
       save('./DEBUG/create_workspace')
    end


            model_params = loadjson(json_model_params);  %#ok<NASGU>
 
 %Perhaps just necessary for calls from Matlab:
            % Check if model_params contain a cell array.
            % Converting that cell array to struct array, because this is
            % probably a conversion error from loadjson.
            fnames = fieldnames(model_params);
            for i = 1: length(fnames)
                if iscell(model_params.(fnames{i}))
                    warning('cell entry found in model params. Converting to stuct.')
                    model_params.(fnames{i}) = myCell2Struct(model_params.(fnames{i}));
                end
            end
            
            
            
            Entities = struct();
            created_list = cell(number,1);
            
            
            % Check if the simulator already has models.
            no_old_fields = 0;
            if ~isempty(fieldnames(obj.entities))
                % If so, how many of the requested kind?
                old_fieldnames = fieldnames(obj.entities);
                for k = 1:numel(old_fieldnames)
                	no_old_fields = no_old_fields + strcmp(obj.entities.(old_fieldnames{k}).type,model_type);
                end
            end
            
            
            % Add their number to all new entitiy counters.
            for entity_counter = 1+no_old_fields:number+no_old_fields
                % Generate entity ID
                Eid = sprintf('%s_%s',model_type,num2str(entity_counter));
                % Create entity
                Entity = eval(sprintf('%s(''%s'',model_params) ',model_type,Eid));
                % Append createt entity to struct
                obj.entities.(Entity.Eid) = Entity;
                
                %Create nested list of created objects
                created_list{entity_counter - no_old_fields} = struct;
                created_list{entity_counter - no_old_fields}.eid = Entity.Eid;
                created_list{entity_counter - no_old_fields}.type = Entity.type;
                created_list{entity_counter - no_old_fields}.rel = {};
                created_list{entity_counter - no_old_fields}.children = {};
            end

            created = savejson('', myCell2Struct(created_list));

            %created = created(2:end-2) %savejson liefert ein set von klammern zu viel, was in python f�r fehler sorgt.

        end
        
%% recall_time = step(obj,current_time, json_inputs)
% <json_inputs> should be a JSONObject mapping all the simulator models 
% (by their <Eid>s) to their specific input parameters. Optionally,
% <inputs> may also contain a parameter <step_size>. In that case, this
% specific step will be executed with a different step_size than that which
% is specified as a simulator property.
        function recall_time = step(obj,current_time, json_inputs)
%     disp('Matlabs Simulator.step method called.')
%     global DEBUG
%     if DEBUG
%        save('./DEBUG/step_workspace')
%     end
    
            % Convert JSON to struct
            inputs = loadjson(json_inputs);
            
            %Set Stepsize to default if not specified in inputs
            if ~isfield(inputs,'step_size')
                this_step_size = obj.step_size;
            else
                this_step_size = inputs.step_size;
            end                 
            
            % Walk through all models and call their step functions.
            EntityNames = fieldnames(obj.entities);
            for i=1:numel(EntityNames)
                if isfield(inputs,EntityNames{i})
                    obj.entities.(EntityNames{i}).step(current_time, this_step_size, inputs.(EntityNames{i}));
                end
            end    

            recall_time = int64(current_time + this_step_size);
    %disp(['Stepped. Current value: ', num2str(new_val)])
        end
        
%% Json_Answer = getData(obj,requested)
% <requested> should be a JSONObject mapping Model-IDs of <Requested_Entities>
% to the names of their <Requested_Fields>.
% For calls from MATLAB, <requested> may also be passed directly as a struct.
% Returns <Json_Answer> as a simple json Object just like <requested>, but
% mapping values to the rewuested fields.
        function Json_Answer = getData(obj,requested)
%     disp('Matlab getData called')
%     global DEBUG
%     if DEBUG
%        save('./DEBUG/getData_workspace')
%     end         

            Answers = struct(); % Predefine answer struct
            
            % Convert JSON to structs
            if ischar(requested)
                RequestStruct = loadjson(requested);
            elseif isstruct(requested)
                RequestStruct = requested;
            else
                error('Unknown format of argument "requested"');
            end
            Requested_Entities = fieldnames(RequestStruct);
            
            % Walking through requestet entities
            for n=1:numel(Requested_Entities) 
                % Check if the entity is valid:
                if isfield(obj.entities,Requested_Entities{n}) 
                    
                    % Read requested fields of this entity
                    Requested_Fields = getfield(RequestStruct,Requested_Entities{n}); %#ok<*GFLD>

                    Fields = struct();
                    % walk through requested fields (mostly just one...)
                    for m = 1:numel(Requested_Fields)
                        if isprop(getfield(obj.entities,Requested_Entities{n}),Requested_Fields{m})
                            % Read requestet entity values
                            Fields = setfield(Fields, Requested_Fields{m},...
                                getfield(getfield(obj.entities,Requested_Entities{n}),...%model object
                                    Requested_Fields{m})); %#ok<*SFLD>
                        else
                            warning('Requested Field %s in Entity %s could not be found.',Requested_Fields{m},Requested_Entities{n});
                        end                        
                    end 
                    
                    % Append read fields to the answer struct
                    Answers = setfield(Answers, Requested_Entities{n}, Fields);
                else
                    warning('Requested Entity %s could not be found.',Requested_Entities{n});
                end
            end  

            % Convert to JSON
            Json_Answer = savejson('', Answers);
    %disp('Matlab getData executed')
        end
        
        
%% destructor: delete(obj)
        function delete(obj)          
              disp(['Destructing Simulator ', obj.simName]);
        end
    end
end

    
